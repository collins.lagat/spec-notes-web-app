const tailwindcss = require('tailwindcss')
const autoprefixer = require('autoprefixer')
const postcssPurgecss = require('@fullhuman/postcss-purgecss')

const purgecss =
    process.env.NODE_ENV === 'production'
        ? [
            postcssPurgecss({
                content: ['./src/**/*.vue', './node_modules/quill/dist/*.js'],
                defaultExtractor: content =>
                    content.match(/[\w-/:]+(?<!:)/g) || [],
            }),
        ]
        : []

module.exports = ({ webpack }) => ({
    plugins: [tailwindcss, ...purgecss, autoprefixer],
})
