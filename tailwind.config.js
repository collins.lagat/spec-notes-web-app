module.exports = {
  theme: {
    extend: {
      colors: {
        'brand-red': '#F38181',
        'brand-green': '#B9E1DC',
        'brand-white': '#FBFBFB',
        'brand-black': '#756C83'
      },
      height: {
        '1/2': '50%',
        '1/4': '25%',
        '2/4': '50%',
        '3/4': '75%',
        '1/5': '20%',
        '2/5': '40%',
        '3/5': '60%',
        '4/5': '80%',
      }
    },
    fontFamily: {
      'sans': ['Roboto', 'sans-serif'],
      'display': ['Fredoka One', 'cursive']
    }
  },
  variants: {},
  plugins: [],
}
