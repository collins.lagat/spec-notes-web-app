import Vue from 'vue'
import App from './App.vue'
import './registerServiceWorker'
import router from './router'
import store from './store'
import * as Sentry from '@sentry/browser';
import * as Integrations from '@sentry/integrations';
import { sentryConfig } from './sentry.config'

// Tailwind
import '@/style/tailwind.scss'

// Sentry
Sentry.init({
	...sentryConfig,
	integrations: [new Integrations.Vue({ Vue, attachProps: true })],
})

// Fontawesome
import { library } from '@fortawesome/fontawesome-svg-core'
import {
	faCircle,
	faBars,
	faHeart,
	faExclamationCircle,
	faTags,
	faBook,
	faStickyNote,
	faSearch,
	faPlus,
	faMinus,
	faArrowLeft,
	faArrowRight,
	faPlusCircle,
	faFolderOpen,
	faTimesCircle,
	faChevronDown,
	faChevronUp,
	faEllipsisV,
	faTrashAlt,
} from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

library.add(
	faCircle,
	faBars,
	faHeart,
	faExclamationCircle,
	faTags,
	faBook,
	faStickyNote,
	faSearch,
	faPlus,
	faMinus,
	faArrowLeft,
	faArrowRight,
	faPlusCircle,
	faFolderOpen,
	faTimesCircle,
	faChevronDown,
	faChevronUp,
	faCircle,
	faEllipsisV,
	faTrashAlt
)

Vue.component('app-icon', FontAwesomeIcon)

Vue.config.productionTip = false

new Vue({
	router,
	store,
	render: h => h(App),
}).$mount('#app')
