interface IPlan {
    name: string;
    price: number;
    billingCycle: 'monthly' | 'yearly'
}

interface ISubscription {
    startDate: Date;
    endDate: Date;
    plan: IPlan
}

interface ISettings {
    favouriteNotebooks: { id: string; name: string }[]
}

export interface IUserState {
    id?: string;
    firstName?: string;
    lastName?: string;
    subscription?: ISubscription,
    settings?: ISettings
}