export interface ITag {
    id?: string;
    name: string;
    description?: string;
}

export interface ITagState {
    tags: ITag[];
}