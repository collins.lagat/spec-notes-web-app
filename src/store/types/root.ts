import { IUserState } from './user';
import { INotebookState } from './notebooks';
import { ITagState } from './tags';

export interface IRoot {
    User: IUserState,
    Notebooks: INotebookState,
    Tags: ITagState
}