import { ITag } from './tags'
import Delta from 'quill-delta'

export interface INote {
    id?: string;
    title: string;
    notebook: {
        id: string;
        name: string;
    }
    tags: ITag[];
    createdOn: Date;
    editedOn: Date;
    body: Delta['ops'];
}

export interface INotebook {
    id?: string;
    name: string;
    description?: string;
    createdOn: Date;
    editedOn: Date;
    notes?: INote[];
}

export interface INotebookState {
    notebooks?: INotebook[],
    currentNotebook?: INotebook
}