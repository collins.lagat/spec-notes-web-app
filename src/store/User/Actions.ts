import { ActionTree } from "vuex";
import { IUserState } from '@/store/types/user'
import { IRoot } from '@/store/types/root';

export const actions: ActionTree<IUserState, IRoot> = {
    pushUserFromDB({ commit }, user: IUserState) {
        commit('addUser', user)
    }
}