import { MutationTree } from "vuex";
import { IUserState } from '@/store/types/user'

export const mutations: MutationTree<IUserState> = {
    addUser(state, user: IUserState) {
        state = user
    },
    emptyUser(state) {
        state = {
            firstName: undefined,
            lastName: undefined,
            subscription: undefined,
        }
    }
}