import { Module } from 'vuex'
import { IUserState } from '@/store/types/user'
import { IRoot } from '@/store/types/root'
import { state } from '@/store/User/State'
import { mutations } from '@/store/User/Mutations'
import { actions } from '@/store/User/Actions'

const namespaced: boolean = true

export const User: Module<IUserState, IRoot> = {
    namespaced,
    state,
    mutations,
    actions
}