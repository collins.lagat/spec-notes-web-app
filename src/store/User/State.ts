import { IUserState } from '@/store/types/user'

export const state: IUserState = {
    firstName: undefined,
    lastName: undefined,
    subscription: undefined,
}