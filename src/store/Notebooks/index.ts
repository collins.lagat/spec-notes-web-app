import { Module } from 'vuex'
import { INotebookState } from '@/store/types/notebooks'
import { IRoot } from '@/store/types/root'
import { state } from '@/store/Notebooks/State'
import { mutations } from '@/store/Notebooks/Mutations'
import { actions } from '@/store/Notebooks/Actions'

const namespaced: boolean = true

export const Notebooks: Module<INotebookState, IRoot> = {
    namespaced,
    state,
    mutations,
    actions
}