import { ActionTree } from "vuex";
import { INotebookState, INotebook } from '@/store/types/notebooks'
import { IRoot } from '@/store/types/root';
import { offlineDB } from '@/offline/db'

export const actions: ActionTree<INotebookState, IRoot> = {
    syncNotebooks({ commit }, notebooks: INotebook[]) {
        commit('resetNotebooks')
        notebooks.forEach((notebook) => commit('addNotebook', notebook))
    },
    syncCurrentNotebook({ commit }, notebook: INotebook) {
        commit('resetCurrentNotebook')
        commit('addCurrentNotebook', notebook)
    },

    watchNotebooks({ dispatch }) {
        return new Promise(async (resolve, reject) => {
            try {

                // Watch Notebook Table

                offlineDB.watch('notebooks').on('create').subscribe(async () => {
                    dispatch('syncNotebooks', await offlineDB.notebooks)
                })
                offlineDB.watch('notebooks').on('update').subscribe(async () => {
                    dispatch('syncNotebooks', await offlineDB.notebooks)
                })
                offlineDB.watch('notebooks').on('delete').subscribe(async () => {
                    dispatch('syncNotebooks', await offlineDB.notebooks)
                })

                // Watch Notes Table

                offlineDB.watch('notes').on('create').subscribe(async () => {
                    dispatch('syncNotebooks', await offlineDB.notebooks)
                })
                offlineDB.watch('notes').on('update').subscribe(async () => {
                    dispatch('syncNotebooks', await offlineDB.notebooks)
                })
                offlineDB.watch('notes').on('delete').subscribe(async () => {
                    dispatch('syncNotebooks', await offlineDB.notebooks)
                })

                // Watch Session Data

                offlineDB.watchSession('currentNotebook').on('create').subscribe(async () => {
                    dispatch('syncCurrentNotebook', offlineDB.currentNotebook)
                })
                offlineDB.watchSession('currentNotebook').on('update').subscribe(async () => {
                    dispatch('syncCurrentNotebook', offlineDB.currentNotebook)
                })
                offlineDB.watchSession('currentNotebook').on('delete').subscribe(async () => {
                    dispatch('syncCurrentNotebook', offlineDB.currentNotebook)
                })

                resolve()
            } catch (error) {
                reject(error)
            }
        })
    }
}