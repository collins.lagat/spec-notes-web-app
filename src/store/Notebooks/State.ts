import { INotebookState } from '@/store/types/notebooks'

export const state: INotebookState = {
    notebooks: undefined,
    currentNotebook: undefined
}