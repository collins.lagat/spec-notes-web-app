import { MutationTree } from "vuex";
import { INotebookState, INotebook } from '@/store/types/notebooks'

export const mutations: MutationTree<INotebookState> = {
    resetNotebooks(state) {
        state.notebooks = []
    },
    resetCurrentNotebook(state) {
        state.currentNotebook = undefined
    },
    addNotebook(state, notebook: INotebook) {
        if (state.notebooks) {
            state.notebooks.push(notebook)
        }
    },
    addCurrentNotebook(state, notebook: INotebook) {
        state.currentNotebook = notebook
    }
}