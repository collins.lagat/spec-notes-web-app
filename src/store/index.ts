import Vue from 'vue'
import Vuex, { StoreOptions } from 'vuex'
import { offlineDB } from '@/offline/db'

// Modules
import { User } from '@/store/User/index'
import { Notebooks } from "@/store/Notebooks/index";
import { Tags } from "@/store/Tags/index";
import { IRoot } from './types/root';

Vue.use(Vuex)

const store: StoreOptions<IRoot> = {
	modules: {
		User,
		Notebooks,
		Tags
	},
	actions: {
		init({ dispatch, commit }) {
			return new Promise(async (resolve, reject) => {
				try {
					// Notebooks

					commit('Notebooks/resetNotebooks')

					const notebooks = await offlineDB.notebooks
					if (notebooks) {
						await dispatch('Notebooks/syncNotebooks', notebooks)
					}

					const currentNotebook = offlineDB.currentNotebook

					await dispatch('Notebooks/syncCurrentNotebook', currentNotebook)

					await dispatch('Notebooks/watchNotebooks')

					// Tags

					commit('Tags/resetTags')

					const tags = await offlineDB.tags
					if (tags) {
						await dispatch('Tags/syncTags', tags)
					}

					await dispatch('Tags/watchTags')

					resolve()
				} catch (error) {
					reject(error)
				}
			})
		},
	}
}

export default new Vuex.Store<IRoot>(store)
