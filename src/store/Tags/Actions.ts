import { ActionTree } from "vuex";
import { ITagState, ITag } from '@/store/types/tags'
import { IRoot } from '@/store/types/root';
import { offlineDB } from '@/offline/db'

export const actions: ActionTree<ITagState, IRoot> = {
    syncTags({ commit }, tags: ITag[]) {
        commit('resetTags')
        tags.forEach((tag) => commit('addTag', tag))
    },
    watchTags({ dispatch }) {
        return new Promise(async (resolve, reject) => {
            try {

                // Watch Tag Table

                offlineDB.watch('tags').on('create').subscribe(async () => {
                    dispatch('syncTags', await offlineDB.tags)
                })
                offlineDB.watch('tags').on('update').subscribe(async () => {
                    dispatch('syncTags', await offlineDB.tags)
                })
                offlineDB.watch('tags').on('delete').subscribe(async () => {
                    dispatch('syncTags', await offlineDB.tags)
                })

                resolve()
            } catch (error) {
                reject(error)
            }
        })
    }
}