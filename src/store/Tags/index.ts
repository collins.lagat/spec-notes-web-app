import { Module } from 'vuex'
import { ITagState } from '@/store/types/tags'
import { IRoot } from '@/store/types/root'
import { state } from '@/store/Tags/State'
import { mutations } from '@/store/Tags/Mutations'
import { actions } from '@/store/Tags/Actions'

const namespaced: boolean = true

export const Tags: Module<ITagState, IRoot> = {
    namespaced,
    state,
    mutations,
    actions
}