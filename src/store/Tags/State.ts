import { ITagState } from '@/store/types/tags'

export const state: ITagState = {
    tags: [],
}