import { MutationTree } from "vuex";
import { ITagState, ITag } from '@/store/types/tags'

export const mutations: MutationTree<ITagState> = {
    resetTags(state) {
        state.tags = []
    },
    addTag(state, tag: ITag) {
        if (state.tags) {
            state.tags.push(tag)
        }
    },
}