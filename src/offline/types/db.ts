import { Subject } from 'rxjs';
import { IDatabaseChange } from 'dexie-observable/api';
import { IUserState } from '@/store/types/user';
import { INotebook, INote } from '@/store/types/notebooks';
import { ITag } from '@/store/types/tags'

export type AvaliableTables = 'user' | 'notebooks' | 'notes' | 'tags'

export type AvailableOperations = 'create' | 'update' | 'delete'

export type TableOperation = Subject<IDatabaseChange>

export type TableOperations = {
    [key in AvailableOperations]: TableOperation
}

export type TableSubscriptions = {
    [key in AvaliableTables]: TableOperations
}

export interface TWatch {
    on: (operation: AvailableOperations) => TableOperation
}

export interface IOfflineDB {

    // User CRUD

    user: Promise<IUserState | undefined>;
    saveUser: (user: IUserState) => Promise<string>
    deleteUser: (user: IUserState) => Promise<void>
    updateUser: (user: IUserState) => Promise<void>

    // Notebooks CRUD

    notebooks: Promise<INotebook[] | undefined>;
    saveNotebook: (notebook: INotebook) => Promise<string>;
    updateNotebook: (notebook: INotebook) => Promise<void>;
    deleteNotebook: (notebook: INotebook) => Promise<void>;

    // Notes CRUD
    saveNote: (note: INote) => Promise<string>;
    updateNote: (note: INote) => Promise<void>;
    deleteNote: (note: INote) => void;

    // Tags CRUD
    tags: Promise<ITag[] | undefined>;
    saveTag: (note: ITag) => Promise<string>;
    updateTag: (note: ITag) => Promise<void>;
    deleteTag: (note: ITag) => Promise<void>;

    // Observables
    watch: (table: AvaliableTables) => TWatch
}

export type sessionData = 'currentNotebook'

type sessionOperations = {
    [key in AvailableOperations]: Subject<void>
}

export type sessionSubscriptions = {
    [key in sessionData]: sessionOperations
}