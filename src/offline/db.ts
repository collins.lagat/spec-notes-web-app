import Dexie from 'dexie'
import 'dexie-observable'
import { v4 as uuid } from 'uuid'
import { Subject } from 'rxjs'
import { IUserState } from '@/store/types/user'
import { INotebook, INote } from '@/store/types/notebooks'
import { ITag } from '@/store/types/tags'
import {
    AvaliableTables,
    TWatch,
    AvailableOperations,
    TableSubscriptions,
    IOfflineDB,
    sessionSubscriptions,
    sessionData,
} from './types/db'
import {
    ICreateChange,
    IUpdateChange,
    IDeleteChange,
} from 'dexie-observable/api'

class Database extends Dexie {
    user: Dexie.Table<IUserState, string>
    notebooks: Dexie.Table<INotebook, string>
    notes: Dexie.Table<INote, string>
    tags: Dexie.Table<ITag, string>

    constructor(dbName: string) {
        super(dbName)

        this.version(1).stores({
            user: 'id, firstName',
            notebooks: 'id, name',
            notes: 'id, title',
            tags: 'id, name',
        })

        this.user = this.table('user')
        this.notebooks = this.table('notebooks')
        this.notes = this.table('notes')
        this.tags = this.table('tags')
    }
}

class XNotesOfflineDB implements IOfflineDB {
    private subscriptions: TableSubscriptions = {
        user: {
            create: new Subject(),
            update: new Subject(),
            delete: new Subject(),
        },
        notebooks: {
            create: new Subject(),
            update: new Subject(),
            delete: new Subject(),
        },
        notes: {
            create: new Subject(),
            update: new Subject(),
            delete: new Subject(),
        },
        tags: {
            create: new Subject(),
            update: new Subject(),
            delete: new Subject(),
        },
    }

    constructor(private db: Database = new Database('XNotes')) {
        this.db.on('changes', changes => {
            changes.forEach(change => {
                const table = change.table as AvaliableTables
                switch (change.type) {
                    case 1: // CREATED
                        this.subscriptions[table].create.next(change)
                        // console.log(
                        //     'An object was created: ',
                        //     change as ICreateChange
                        // )
                        break
                    case 2: // UPDATED
                        this.subscriptions[table].update.next(
                            change as IUpdateChange
                        )
                        // console.log(
                        //     'An object with was updated with modifications: ',
                        //     (change as IUpdateChange).mods
                        // )
                        break
                    case 3: // DELETED
                        this.subscriptions[table].delete.next(
                            change as IDeleteChange
                        )
                        // console.log(
                        //     'An object was deleted: ',
                        //     change as IDeleteChange
                        // )
                        break
                }
            })
        })
        this.db.notebooks.get('1111-7777-0000-1111').then(resp => {
            if (!resp) {
                this.saveNotebook(this.defaultNotebook)
            }
        })
    }

    // Observables

    watch = (table: AvaliableTables): TWatch => {
        const choosenTable = this.subscriptions[table]
        return {
            on: (operation: AvailableOperations) => choosenTable[operation],
        }
    }

    // Getters

    get user(): Promise<IUserState | undefined> {
        return new Promise(async (resolve, reject) => {
            try {
                const user = (await this.db.user.toArray())[0]
                resolve(user)
            } catch (error) {
                reject(error)
            }
        })
    }

    get notebooks(): Promise<INotebook[] | undefined> {
        return new Promise(async (resolve, reject) => {
            try {
                const notebooks = await this.db.notebooks.toArray()
                const notes = await this.db.notes.toArray()

                const response: INotebook[] = notebooks.map(notebook => {
                    if (notebook.id) {
                        const notesInNotebook: INote[] = notes.filter(note => {
                            return notebook.id === note.notebook.id
                        })

                        notebook.notes = notesInNotebook
                        return notebook
                    }
                    return notebook
                })
                resolve(response)
            } catch (error) {
                reject(error)
            }
        })
    }

    get tags(): Promise<ITag[] | undefined> {
        return new Promise(async (resolve, reject) => {
            try {
                const tags = await this.db.tags.toArray()
                resolve(tags)
            } catch (error) {
                reject(error)
            }
        })
    }

    private get notes(): Promise<INote[] | undefined> {
        return new Promise(async (resolve, reject) => {
            try {
                const notes = await this.db.notes.toArray()
                resolve(notes)
            } catch (error) {
                reject(error)
            }
        })
    }

    // Methods

    // 1. User

    saveUser(user: IUserState): Promise<string> {
        return new Promise(async (resolve, reject) => {
            try {
                // Is First Name Valid?

                if (user.firstName === undefined || user.firstName.length < 3)
                    return reject(new Error('Invalid First Name'))

                // Does user exist (only one user is allowed in the db at a time)

                const existingUser = await this.user

                if (existingUser !== undefined) {
                    await this.updateUser(user)
                    resolve()
                    return
                }

                // Does user have id?

                if (!user.id) user.id = uuid()

                // Save user

                const id = await this.db.user.add(user)
                resolve(id)
            } catch (error) {
                reject(error)
            }
        })
    }

    updateUser(user: IUserState): Promise<void> {
        return new Promise(async (resolve, reject) => {
            try {
                // Is First Name Valid?

                if (user.firstName === undefined || user.firstName.length < 3)
                    return reject(new Error('Invalid First Name'))

                // Check if user exists

                const existingUser = await this.user

                if (existingUser !== undefined) {
                    //  Update User

                    user.id = existingUser.id
                    await this.db.user.put(user)
                    resolve()
                    return
                } else {
                    reject(new Error('No User Found'))
                }
            } catch (error) {
                reject(error)
            }
        })
    }

    deleteUser(user: IUserState): Promise<void> {
        return new Promise(async (resolve, reject) => {
            try {
                // Check if id Exists

                if (user.id === undefined)
                    return reject(new Error('User ID is undefined'))

                // Delete User

                await this.db.user.delete(user.id)
                resolve()
            } catch (error) {
                reject(error)
            }
        })
    }

    // 2. Notebook
    saveNotebook(notebook: INotebook): Promise<string> {
        return new Promise(async (resolve, reject) => {
            try {
                // Check Notebook Name Length

                if (notebook.name.length < 3)
                    return reject(new Error('Title is too short'))

                // Check if Notebook already exists

                if (notebook.id !== undefined) {
                    const existingNotebook = await this.db.notebooks.get(
                        notebook.id
                    )
                    if (existingNotebook !== undefined) {
                        await this.updateNotebook(notebook)
                        resolve()
                        return
                    }
                }

                // Does notebook have an id?

                if (!notebook.id) notebook.id = uuid()

                // Save notebook

                const id = await this.db.notebooks.add(notebook)
                resolve(id)
            } catch (error) {
                reject(error)
            }
        })
    }

    updateNotebook(notebook: INotebook): Promise<void> {
        return new Promise(async (resolve, reject) => {
            try {
                // Check Notebook Name Length

                if (notebook.name.length < 3)
                    return reject(new Error('Name is too short'))

                // Check if Notebook has an id

                if (notebook.id === undefined)
                    return reject(new Error('Notebook has no ID'))

                // Check if Notebook already exists

                const existingNotebook = await this.db.notebooks.get(
                    notebook.id
                )
                if (existingNotebook === undefined)
                    return reject(new Error('Notebook does not Exist'))

                // Update notebook

                await this.db.notebooks.put(notebook)
                resolve()
            } catch (error) {
                reject(error)
            }
        })
    }

    deleteNotebook(notebook: INotebook): Promise<void> {
        return new Promise(async (resolve, reject) => {
            try {
                // Check if Notebook id Exists

                if (notebook.id === undefined)
                    return reject(new Error('Notebook ID is undefined'))

                // Delete All Notes within Notebook
                const notebooks = await this.notebooks
                if (notebooks && notebooks.length > 0) {
                    const correctNotebook = notebooks.find(
                        _notebook => _notebook.id === notebook.id
                    )
                    if (correctNotebook && correctNotebook.notes) {
                        const { notes } = correctNotebook

                        for (let index = 0; index < notes.length; index++) {
                            const note = notes[index]
                            await this.deleteNote(note)
                        }
                    }
                }

                // Delete Notebook

                await this.db.notebooks.delete(notebook.id)
                resolve()
            } catch (error) {
                reject(error)
            }
        })
    }

    // 3. Note

    saveNote(note: INote): Promise<string> {
        return new Promise(async (resolve, reject) => {
            try {
                // Check Note Title Length

                if (note.title.length < 1)
                    return reject(new Error('Title is too short'))

                // Check if Note already exists

                if (note.id !== undefined) {
                    const existingNote = await this.db.notes.get(note.id)
                    if (existingNote !== undefined) {
                        await this.updateNote(note)
                        resolve()
                        return
                    }
                }

                // Does Note have an id?

                if (!note.id) note.id = uuid()

                // Save notebook

                const id = await this.db.notes.add(note)
                resolve(id)
            } catch (error) {
                reject(error)
            }
        })
    }

    updateNote(note: INote): Promise<void> {
        return new Promise(async (resolve, reject) => {
            try {
                // Check Note Title Length

                if (note.title.length < 3)
                    return reject(new Error('Title is too short'))

                // Check if Note has an id

                if (note.id === undefined)
                    return reject(new Error('Note has no ID'))

                // Check if Note already exists

                const existingNote = await this.db.notes.get(note.id)
                if (existingNote === undefined)
                    return reject(new Error('Note does not Exist'))

                // Update Note

                await this.db.notes.put(note)
                resolve()
            } catch (error) {
                reject(error)
            }
        })
    }

    deleteNote(note: INote): Promise<void> {
        return new Promise(async (resolve, reject) => {
            try {
                // Check if Note id Exists

                if (note.id === undefined)
                    return reject(new Error('Note ID is undefined'))

                // Delete Note

                await this.db.notes.delete(note.id)
                resolve()
            } catch (error) {
                reject(error)
            }
        })
    }

    // 3. Note

    saveTag(tag: ITag): Promise<string> {
        return new Promise(async (resolve, reject) => {
            try {
                // Check Tag Name Length

                if (tag.name.length < 1)
                    return reject(new Error('Name is too short'))

                // Check if Tag already exists

                if (tag.id) {
                    const existingTag = await this.db.tags.get(tag.id)
                    if (existingTag) {
                        await this.updateTag(tag)
                        resolve()
                        return
                    }
                }

                // Does Tag have an id?

                if (!tag.id) tag.id = uuid()

                // Save Tag

                const id = await this.db.tags.add(tag)
                resolve(id)
            } catch (error) {
                reject(error)
            }
        })
    }

    updateTag(tag: ITag): Promise<void> {
        return new Promise(async (resolve, reject) => {
            try {
                // Check Tag Name Length

                if (tag.name.length < 3)
                    return reject(new Error('Name is too short'))

                // Check if Tag has an id

                if (!tag.id) return reject(new Error('Tag has no ID'))

                // Check if Tag already exists

                const existingTag = await this.db.tags.get(tag.id)
                if (!existingTag) return reject(new Error('Tag does not Exist'))

                // Update Note

                await this.db.tags.put(tag)
                resolve()
            } catch (error) {
                reject(error)
            }
        })
    }

    deleteTag(tag: ITag): Promise<void> {
        return new Promise(async (resolve, reject) => {
            try {
                // Check if Tag id Exists

                if (tag.id === undefined)
                    return reject(new Error('Tag ID is undefined'))

                // Remove Tag from all notes with tag
                const notes = await this.notes
                if (notes && tag.id) {
                    const notesWithTag = notes.filter((note => note.tags.some(_tag => _tag.id === tag.id)))

                    for (let index = 0; index < notesWithTag.length; index++) {
                        const note = notesWithTag[index]

                        const tagIndex = note.tags.findIndex(_tag => _tag.id === tag.id)

                        note.tags.splice(tagIndex, 1)

                        await this.updateNote(note)
                    }

                }
                // Delete Tag

                await this.db.tags.delete(tag.id)
                resolve()
            } catch (error) {
                reject(error)
            }
        })
    }

    // Manage Session Data

    private defaultNotebook: INotebook = {
        id: '1111-7777-0000-1111',
        name: 'Inbox',
        createdOn: new Date(),
        editedOn: new Date(),
        description: ' Default Notebook',
        notes: [],
    }

    private sessionSubscriptions: sessionSubscriptions = {
        currentNotebook: {
            create: new Subject(),
            update: new Subject(),
            delete: new Subject(),
        },
    }

    get currentNotebook(): INotebook {
        const notebook = sessionStorage.getItem('currentNotebook')
        if (notebook) {
            return JSON.parse(notebook) as INotebook
        }
        return this.defaultNotebook
    }

    saveCurrentNotebook(notebook: INotebook) {
        sessionStorage.setItem('currentNotebook', JSON.stringify(notebook))
        this.sessionSubscriptions.currentNotebook.create.next()
        this.sessionSubscriptions.currentNotebook.update.next()
    }

    deleteCurrentNotebook() {
        sessionStorage.removeItem('currentNotebook')
        this.sessionSubscriptions.currentNotebook.delete.next()
    }

    watchSession(metadata: sessionData) {
        return {
            on: (operation: AvailableOperations) =>
                this.sessionSubscriptions[metadata][operation],
        }
    }
}

export const offlineDB = new XNotesOfflineDB()
