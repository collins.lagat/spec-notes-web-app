import Vue from 'vue'
import VueRouter from 'vue-router'
import { RouteConfig } from 'vue-router/types/index'
import PageNotFound from '@/404.vue'
import Home from '@/app/Home/Home.vue'
import GettingStarted from '@/app/User/GettingStarted.vue'
import GuestSignUp from '@/app/User/GuestSignUp.vue'
import Notebook from '@/app/Editor/Notebook.vue'
import Note from '@/app/Editor/Note.vue'
import Tag from '@/app/Editor/Tag.vue'
import Editor from '@/app/Editor/Editor.vue'
import { Auth } from '@/auth/auth.service'

Vue.use(VueRouter)

const userExists = async (): Promise<boolean> => {
	if (Auth.currentUser) {
		return true
	} else {
		await Auth.syncDB()
		return Auth.currentUser ? true : false
	}
}

const routes: RouteConfig[] = [
	{
		path: '/',
		name: 'home',
		component: Home,
		beforeEnter: async (to, from, next) => {
			const state = await userExists()

			if (state) {
				next({ name: 'notebook' })
			} else {
				next()
			}
		},
	},
	{
		path: '/getting-started',
		name: 'getting-started',
		component: GettingStarted,
	},
	{
		path: '/getting-started/guest',
		name: 'guest-signup',
		component: GuestSignUp,
	},
	{
		path: '/notebook',
		name: 'notebooks',
		redirect: {
			name: 'notebook',
			params: {
				NotebookID: '1111-7777-0000-1111',
			},
		},
		beforeEnter: async (to, from, next) => {
			const state = await userExists()

			if (state) {
				next()
			} else {
				next({ name: 'home' })
			}
		},
	},
	{
		path: '/notebook/:NotebookID/',
		component: Editor,
		children: [
			{
				path: '/',
				name: 'notebook',
				component: Notebook,
			},
		],
		beforeEnter: async (to, from, next) => {
			const state = await userExists()

			if (state) {
				next()
			} else {
				next({ name: 'home' })
			}
		},
	},
	{
		path: '/tags/:TagID/',
		component: Editor,
		children: [
			{
				path: '/',
				name: 'tag',
				component: Tag,
			},
		],
		beforeEnter: async (to, from, next) => {
			const state = await userExists()

			if (state) {
				next()
			} else {
				next({ name: 'home' })
			}
		},
	},
	{
		path: '/notebook/:NotebookID/:NoteID',
		name: 'note',
		component: Note,
		beforeEnter: async (to, from, next) => {
			const state = await userExists()

			if (state) {
				next()
			} else {
				next({ name: 'home' })
			}
		},
	},
	{
		path: '*',
		name: '404',
		component: PageNotFound,
	},
]

const router = new VueRouter({
	routes,
})

export default router
