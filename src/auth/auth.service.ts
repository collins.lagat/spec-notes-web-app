import { Store } from "vuex";
import { offlineDB } from "@/offline/db";
import { IUserState } from '@/store/types/user';
import { IRoot } from '@/store/types/root';
import VuexStore from '@/store/index'

class AuthService {
    private VUEX: Store<IRoot> | undefined;
    private VUEX_USER: IUserState | undefined;
    private DB_USER: IUserState | undefined;

    constructor(store: Store<IRoot>) {
        this.VUEX_USER = store.state.User
        this.VUEX = store
        this.syncDB()
    }

    get currentUser(): IUserState | null {
        if (this.VUEX_USER === undefined) return null
        if (this.DB_USER) {
            return this.VUEX_USER
        } else {
            return null
        }
    }

    async syncDB(): Promise<void> {
        const user = await offlineDB.user
        if (user === undefined) return

        if (this.VUEX) await this.VUEX.dispatch('User/pushUserFromDB', user)
        this.DB_USER = user
    }
}

export const Auth = new AuthService(VuexStore)