export interface SlideOverRefs {
    [key: string]: Vue | Element | Vue[] | Element[];
    slideover: Element
    menu: Element
    overlay: Element
}