export interface AddTagForm {
    name: string;
    description?: string;
}

export interface AddTagFormRefs {
    [key: string]: Vue | Element | Vue[] | Element[];
    overlay: Element;
    addTagForm: Element;
    formWrapper: Element;
}