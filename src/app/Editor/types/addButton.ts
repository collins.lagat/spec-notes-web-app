export interface AddButtonRefs {
    [key: string]: Vue | Element | Vue[] | Element[];
    overlay: Element;
    addNotebook: Element;
    addNote: Element;
    addButton: Element;
    addTag: Element;
}