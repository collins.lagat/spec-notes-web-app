export interface AddNotebookFormRefs {
    [key: string]: Vue | Element | Vue[] | Element[];
    overlay: Element;
    addNotebookForm: Element;
    formWrapper: Element;
}

export interface AddNotebookForm {
    name: string;
    description?: string;
}