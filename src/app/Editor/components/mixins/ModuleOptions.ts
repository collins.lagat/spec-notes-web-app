import Vue from 'vue'
import Component from 'vue-class-component'

@Component
export class ModuleOptionsMixin extends Vue {
    // Data

    open: boolean = false
    isPopUpOpen: boolean = false

    // Methods

    toggleOpen() {
        this.open = !this.open
    }

    closePopup() {
        this.isPopUpOpen = false
    }

    openPopup() {
        this.isPopUpOpen = true
        this.toggleOpen()
    }
}
