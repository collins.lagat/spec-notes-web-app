import Component, { mixins } from 'vue-class-component'
import { Prop } from 'vue-property-decorator'
import { validationMixin, Validation } from 'vuelidate'
import { sameAs, not } from 'vuelidate/lib/validators'
import { ValidationProperties } from 'vue/types/vue'

const validations = {
    name: {
        sameAs: sameAs('existingName'),
        not: not(sameAs('protectedNotebook')),
    },
    existingName: {},
    protectedNotebook: {},
}

@Component({
    validations
})
export class ModulePopupMixin extends mixins(validationMixin) {
    // Props

    @Prop() open!: boolean

    // Data

    // Use To confirm if thats the users intended action
    name: string | null = null
    existingName: string | null = null
    protectedNotebook: string = 'Inbox'

    // Methods

    closeOpen() {
        this.$emit('open')
    }

    // To be customised to specific needs
    invalid(): boolean {
        return false
    }

    confirmAction() {
        if (!this.invalid()) {
            this.$emit('confirmAction')
            this.closeOpen()
        }
    }
}
